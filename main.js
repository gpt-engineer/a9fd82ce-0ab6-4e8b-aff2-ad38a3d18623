document.getElementById('add-to-cart').addEventListener('click', function() {
    var url = document.getElementById('url').value;
    var name = document.getElementById('name').value;
    var price = document.getElementById('price').value;
    var currency = document.getElementById('currency').value;

    var cart = document.getElementById('cart').getElementsByTagName('tbody')[0];
    var row = cart.insertRow();

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = name;
    cell2.innerHTML = price;
    cell3.innerHTML = currency;
    cell4.innerHTML = '<button class="p-2 bg-red-500 text-white rounded">Remove</button>';

    updateSummary();
});

function updateSummary() {
    var cart = document.getElementById('cart').getElementsByTagName('tbody')[0];
    var rows = cart.getElementsByTagName('tr');

    var total = 0;
    for (var i = 0; i < rows.length; i++) {
        total += parseFloat(rows[i].cells[1].innerText);
    }

    var savings = total * 0.1;

    document.getElementById('total').innerText = 'Total: ' + total.toFixed(2);
    document.getElementById('savings').innerText = 'Savings: ' + savings.toFixed(2);
}
